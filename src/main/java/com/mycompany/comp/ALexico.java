/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.comp;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ALexico {
    String input;

    public ALexico(String input) {
        this.input = input;
    }
    
    public String analizador() {
        Pattern pattern = Pattern.compile("[a-zA-Z]{4}\\d{6}\\w{3}");
        Matcher matcher = pattern.matcher(input);
        if(matcher.find())
        {
            String match = matcher.group();
            return "RFC válido: " + match;
        }
        else
            return "RFC inválido";
    }
}
